/*
 * CallButton.cpp
 *
 *  Created on: Feb 12, 2018
 *      Author: swalmsley
 */

#include <cstring>
#include <cstdlib>
#include "CallButton.h"

CallButton::CallButton() {
	this->pressed = false;
	this->dest_floors = 0;
	this->dest_floors_cnt = 0;
}

CallButton::~CallButton() {
	// nothing to clean up
}

void CallButton::PressButton(int* floors_in, int floors_cnt_in) {
	this->pressed = true;
	this->dest_floors = (int *)calloc(floors_cnt_in, sizeof(int));
	for(int i = 0; i < floors_cnt_in; i++) {
		this->dest_floors[i] = floors_in[i];
	}
	this->dest_floors_cnt = floors_cnt_in;
}

void CallButton::ButtonServiced() {
	this->pressed = false;
	delete this->dest_floors;
	this->dest_floors = 0;
	this->dest_floors_cnt = 0;
}


