/*
 * CallButton.h
 *
 *  Created on: Feb 12, 2018
 *      Author: swalmsley
 */

#ifndef CALLBUTTON_H_
#define CALLBUTTON_H_

class CallButton {
public:
	bool pressed;
	int* dest_floors;
	int dest_floors_cnt;

public:
	CallButton();
	virtual ~CallButton();

	void PressButton(int* dest_floors_in, int dest_floors_cnt_in);
	void ButtonServiced();
};

#endif /* CALLBUTTON_H_ */
