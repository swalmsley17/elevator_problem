/*
 * Elevator.h
 *
 *  Created on: Feb 12, 2018
 *      Author: swalmsley
 */

#ifndef ELEVATOR_H_
#define ELEVATOR_H_

#include <iostream>

using namespace std;

enum ELEVATOR_DOOR_STATUS {
	ELEVATOR_DOORS_OPEN,
	ELEVATOR_DOORS_CLOSED
};

enum ELEVATOR_STATUS {
	ELEVATOR_IN_SERVICE,
	ELEVATOR_OUT_OF_ORDER
};

class Elevator {
private:
	int MAX_FLOORS;
	int cur_floor;

	ELEVATOR_DOOR_STATUS door_status;
	ELEVATOR_STATUS status;

public:
	Elevator(int max_floor = 2); //default value of 2, I don't image a 1 story building needing an elevator --SAW
	virtual ~Elevator();

	bool MoveElevatorUp();
	bool MoveElevatorDown();
	int GetCurrentFloor();
	int GetMaxFloor();

	bool OpenElevatorDoors();
	bool CloseElevatorDoors();
	ELEVATOR_DOOR_STATUS GetElevatorDoorStatus();

	void SetElevatorInService();
	void SetElevatorOutOfOrder();
	ELEVATOR_STATUS GetElevatorStatus();

};

#endif /* ELEVATOR_H_ */
