/*
 * ElevatorControl.cpp
 *
 *  Created on: Feb 12, 2018
 *      Author: swalmsley
 */

#include "ElevatorController.h"

ElevatorController::ElevatorController(Elevator e_in) {
	this->e = e_in;
	this->cur_dest = this->e.GetCurrentFloor();
	this->direction = STOPPED;

	this->buttons = new CallButton[this->e.GetMaxFloor()];
	this->dest_list = new DestinationList();
	this->term_thread_flag = false;
}

ElevatorController::~ElevatorController() {
	//nothing to clean up
}

/*
 * Puts the Elevator in Service
 */
void ElevatorController::StartElevator() {
	this->mtx.lock();

	cout << "Setting Elevator In-Service" << endl;
	this->e.SetElevatorInService();

	this->mtx.unlock();
}

/*
 * Takes the Elevator Out-Of-Service
 */
void ElevatorController::ServiceElevator() {
	this->mtx.lock();

	cout << "Setting Elevator Out-Of-Order" << endl;
	this->e.SetElevatorOutOfOrder();

	this->mtx.unlock();
}

/*
 * Calls Elevator to a specific floor, and stores the destination data that will be used when the elevator arrives to that floor
 */
bool ElevatorController::CallElevator(int source_floor, int* dest_floors, int dest_floors_cnt) {
	this->mtx.lock();

	//make sure we don't call for floors that don't exist
	if(source_floor > this->e.GetMaxFloor() || source_floor < 1) {
		this->mtx.unlock();
		return false;
	}

	//if button is already press, ignore new presses
	if(!this->buttons[source_floor -1].pressed)
	{
		this->buttons[source_floor -1].PressButton(dest_floors, dest_floors_cnt);
		this->dest_list->AddDestination(source_floor);
	}

	this->mtx.unlock();
	return true;
}

/*
 * Prints a quick summary of the elevators current state
 */
void ElevatorController::PrintSummary()
{
	this->mtx.lock();

	cout << "Elevator Controller System Summary:" << endl;
	cout << "\t" << this->e.GetMaxFloor() << " Stories : Current Floor = " << this->e.GetCurrentFloor() << endl;
	cout << "\tDirection = ";
	switch(this->direction)
	{
		case STOPPED:
			cout << "Stopped";
			break;
		case UPWARDS:
			cout << "Upwards";
			break;
		case DOWNWARDS:
			cout << "Downwards";
	}
	cout << endl << endl;;

	cout << "\tStatus: ";
	switch(this->e.GetElevatorStatus())
	{
		case ELEVATOR_IN_SERVICE:
			cout << "In Service";
			break;

		case ELEVATOR_OUT_OF_ORDER:
			cout << "Out of Order";
			break;
	}
	cout << endl;

	cout << "\tDoors are : ";
	switch(this->e.GetElevatorDoorStatus())
	{
		case ELEVATOR_DOORS_OPEN:
			cout << "Open";
			break;
		case ELEVATOR_DOORS_CLOSED:
			cout << "Closed";
			break;
	}
	cout << endl << endl;

	this->mtx.unlock();
}

/*
 * Starts the thread that handles moving the elevators to destination floors
 */
void ElevatorController::StartControllerThread() {
	this->con_thread = thread(&ElevatorController::ControllerThreadFunc, this);

}

/*
 * Kills the thread that handles moving elevators to destination floors
 * This is used when stopping the Elevator controller
 */
void ElevatorController::KillControllerThread() {
	this->mtx.lock();

	this->term_thread_flag = true;

	this->mtx.unlock();
	this->con_thread.join();
}

/*
 * Function that is passed for the thread to operate within, handles moving the elevator to the correct floors
 */
void ElevatorController::ControllerThreadFunc(){
	bool long_sleep = false;
	while(1)
	{

		this->mtx.lock();

		//check if thread needs to terminate
		if(this->term_thread_flag)
		{
			this->term_thread_flag = false;
			this->mtx.unlock();
			break;
		}

		//if we are in an error state, sleep a little longer than normal
		if(!ElevatorErrorCheck())
		{
			long_sleep = true;
		}
		//if we are stopped and have a destination
		else if(!this->dest_list->isEmpty())
		{
			int dest;
			this->dest_list->PeekFirst(&dest);

			//destination is current floor
			if(this->e.GetCurrentFloor() == dest)
			{
				cout << endl;
				cout << "Reached Destination : Floor " << this->e.GetCurrentFloor() << endl;
				this->e.OpenElevatorDoors();
				this->dest_list->RemoveFirst(); //remove cur floor as destination
				if(this->buttons[this->e.GetCurrentFloor() -1].pressed)
				{
					for(int i = 0; i < this->buttons[this->e.GetCurrentFloor() -1].dest_floors_cnt; i++)
					{
						//Make sure we don't add current floor to List if it is selected
						if(this->buttons[this->e.GetCurrentFloor() -1].dest_floors[i] != this->e.GetCurrentFloor())
							this->dest_list->AddDestination(this->buttons[this->e.GetCurrentFloor() -1].dest_floors[i]);
					}
					this->buttons[this->e.GetCurrentFloor() -1].ButtonServiced();
				}
				this->dest_list->SortNextDestination(this->e.GetCurrentFloor(), this->e.GetMaxFloor(), this->direction);
				this->e.CloseElevatorDoors();
				cout << endl;
			}

			//destination is above current floor
			else if(this->e.GetCurrentFloor() < dest)
			{
				this->dest_list->SortNextDestination(this->e.GetCurrentFloor(), this->e.GetMaxFloor(), this->direction);
				cout << "Elevator moving up to floor " << this->e.GetCurrentFloor()+1 << endl;
				this->direction = UPWARDS;
				this->e.MoveElevatorUp();
			}

			//destination is below current floor
			else
			{
				this->dest_list->SortNextDestination(this->e.GetCurrentFloor(), this->e.GetMaxFloor(), this->direction);
				this->direction = DOWNWARDS;
				cout << "Elevator moving down to floor " << this->e.GetCurrentFloor()-1 << endl;
				this->e.MoveElevatorDown();
			}
		}
		else
		{
			this->direction = STOPPED;
		}

		this->mtx.unlock();

		//long_sleep is set when we are Out_Of_Order
		if(long_sleep)
		{
			this_thread::sleep_for(chrono::seconds(5));
			long_sleep = false;
		}
		else
			this_thread::sleep_for(chrono::seconds(1));
	}
}

/*
 * Checks the elevator status to make sure we don't move it when it's in an unmovable state
 */
bool ElevatorController::ElevatorErrorCheck()
{
	if(this->e.GetElevatorDoorStatus() == ELEVATOR_DOORS_OPEN)
	{
		cout << "ERROR: Elevator cannot move while door is open!" << endl;
		return false;
	}

	if(this->e.GetElevatorStatus() == ELEVATOR_OUT_OF_ORDER)
	{
		cout << "ERROR: Elevator cannot move while out of order!" << endl;
		return false;
	}
	return true;
}

