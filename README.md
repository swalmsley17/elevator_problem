# README #

### How do I get set up? ###
Compile using the provided makefile  (I worked on this using linux using g++, but I see no reason cygwin/mingw g++ on windows shouldn't work)
Should you decide to compile w/o makefile, make sure to add the -lpthread flag to the compiler

Run with './ElevatorProblem' in terminal

### How it works ###
Upon launching the application you should be prompted with how many floors you'd like your building to have, select your floor count and press enter.
You should now see a printed menu:
	C - Calls and elevator to a selected floor, you may then choose that floors to be destinations floor (what button will be pressed when the elevator arrives to the called floor)
	* - Toggles the elevator status, if the elevator is out of order it won't service called floors (or any destination floors on the queue), but it can still take call requests and will service them when back in service.
	P - Prints a quick summary of the Elevators current state
	? - Displays the User Menu
	X - Exits the program

Note: If you would like to run my debug/test main, simply uncomment this line in main.cpp "//#define DEBUG"
		The debug function runs indefinatly, adding more requests to the ElevatorController, so you will have to force close the application when you are done.

### Future Work/Shortcomings ###
The frontend of this program does not allow the user to select multiple destination floors when calling the elevator, that said the test function demonstrates the back-ends capabilliity to handle that situation.
Currently the program ignores duplicate Calls from the same floor, I think that servicing those calls to make sure new destination floors are taken care of should be added.
An acutal GUI would be nice, since I have the elevator control on it's own thread it should be incredibly easy to put a GUI ontop of it.
