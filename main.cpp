/*
 * main.cpp
 *
 *  Created on: Feb 12, 2018
 *      Author: swalmsley
 */
#include "./ElevatorController/ElevatorController.h"
#include "./Elevator/Elevator.h"

//#define DEBUG

static void ElevatorUserControl();
static void PrintUserControlMenu();

static void ElevatorControllerTest();
int main()
{
#ifdef DEBUG
	ElevatorControllerTest();
#else
	ElevatorUserControl();
#endif

}

static void ElevatorUserControl() {

	int stories;
	cout << "Enter Number of Stories (2 and up): ";
	cin >> stories;
	while(stories < 2)
	{
		cin.clear();
		cin.ignore();
		cout << "Enter Number of Stories (2 and up): ";
		cin >> stories;
	}

	Elevator elev(stories);
	ElevatorController econ(elev);
	econ.StartControllerThread();
	econ.StartElevator();

	char choice;
	bool estatus_flag = false,exit_flag = false;

	PrintUserControlMenu();
	while(!exit_flag)
	{
		int source_floor = 0, dest_floor[] = {0};
		cin >> choice;
		switch(choice)
		{
			//Call Elevator
			case 'C':
				while(source_floor < 1 || source_floor > stories)
				{
					cout << "Call from Floor: ";
					cin >> source_floor;
					cout << endl;
					cin.clear();
					cin.ignore();
				}

				while(dest_floor[0] < 1 || dest_floor[0] > stories)
				{
					cout << "Destination Floor: ";
					cin >> dest_floor[0];
					cout << endl;
					cin.clear();
					cin.ignore();
				}

				econ.CallElevator(source_floor, (int*)dest_floor, 1);
				break;
			//Toggle elevator status
			case '*':
				if(!estatus_flag)
					econ.ServiceElevator();
				else
					econ.StartElevator();
				estatus_flag = !estatus_flag;
				break;
			//Print Controller Summary
			case 'P':
				econ.PrintSummary();
				break;
			//print options
			case '?':
				PrintUserControlMenu();
				break;
			//Exit
			case 'X':
				exit_flag = true;
				break;

			default:
				cout << "NOT A VALID OPTION!" << endl;
		}
	}
	econ.ServiceElevator();
	econ.KillControllerThread();

}

static void PrintUserControlMenu() {
	cout << endl;
	cout << "[C] - Call Elevator" << endl;
	cout << "[*] - Toggle Elevator Status" << endl;
	cout << "[P] - Print Controller Summary" << endl;
	cout << endl;
	cout << "[?] - Display Menu" << endl;
	cout << "[X] - Exit" << endl;
	cout << endl;
	cout << "Choice: ";
}

static void ElevatorControllerTest() {
	Elevator elev(5);
	ElevatorController econ(elev);

	econ.PrintSummary();

	int dest_floors[] = {5, 3, 1};
	int alt_floors[] = {2, 4};

	econ.StartControllerThread();

	this_thread::sleep_for(chrono::seconds(2));

	econ.StartElevator();

	bool flag = false;
	while(1)
	{
		this_thread::sleep_for(chrono::seconds(8));

		if(!flag)
		{
			econ.CallElevator(1, (int*)dest_floors, sizeof(dest_floors)/sizeof(dest_floors[0]));
		}

		else
		{
			econ.CallElevator(4, (int*)alt_floors, sizeof(alt_floors)/sizeof(alt_floors[0]));
		}
		flag = !flag;
	}
}
